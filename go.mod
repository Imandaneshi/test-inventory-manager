module gitlab.com/Imandaneshi/test-inventory-manager

go 1.16

require (
	github.com/99designs/gqlgen v0.13.0
	github.com/gin-gonic/contrib v0.0.0-20201101042839-6a891bf89f19
	github.com/gin-gonic/gin v1.7.4
	github.com/go-playground/validator/v10 v10.9.0 // indirect
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/google/uuid v1.3.0
	github.com/json-iterator/go v1.1.11 // indirect
	github.com/kamva/mgm/v3 v3.4.1
	github.com/mattn/go-isatty v0.0.13 // indirect
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/modern-go/reflect2 v1.0.1 // indirect
	github.com/sarulabs/di v2.0.0+incompatible
	github.com/sirupsen/logrus v1.8.1
	github.com/ugorji/go v1.2.6 // indirect
	github.com/urfave/cli/v2 v2.3.0
	github.com/vektah/gqlparser/v2 v2.1.0
	github.com/xorcare/pointer v1.1.0 // indirect
	go.mongodb.org/mongo-driver v1.7.0
	golang.org/x/crypto v0.0.0-20210817164053-32db794688a5
	golang.org/x/sys v0.0.0-20210823070655-63515b42dcdf // indirect
	golang.org/x/text v0.3.7 // indirect
	google.golang.org/protobuf v1.27.1 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
)
