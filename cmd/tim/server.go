package main

import (
	"context"
	"fmt"

	"github.com/99designs/gqlgen/graphql/handler"
	"github.com/99designs/gqlgen/graphql/playground"
	"github.com/gin-gonic/gin"
	"github.com/urfave/cli/v2"

	"gitlab.com/Imandaneshi/test-inventory-manager/api/graph"
	"gitlab.com/Imandaneshi/test-inventory-manager/api/graph/generated"
	"gitlab.com/Imandaneshi/test-inventory-manager/config"
	"gitlab.com/Imandaneshi/test-inventory-manager/pkg/directives"
	"gitlab.com/Imandaneshi/test-inventory-manager/pkg/services"
	"gitlab.com/Imandaneshi/test-inventory-manager/pkg/services/registry"
)

func GinContextToContextMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		ctx := context.WithValue(c.Request.Context(), "GinContextKey", c)
		c.Request = c.Request.WithContext(ctx)
		c.Next()
	}
}

// Defining the Graphql handler
func graphqlHandler() gin.HandlerFunc {
	// NewExecutableSchema and Config are in the generated.go file
	// Resolver is in the resolver.go file
	h := handler.NewDefaultServer(generated.NewExecutableSchema(generated.Config{Resolvers: &graph.Resolver{},
		Directives: generated.DirectiveRoot{
			LoginRequired: directives.Auth,
			AdminRequired: directives.AdminCheck,
		}}))

	return func(c *gin.Context) {
		h.ServeHTTP(c.Writer, c.Request)
	}
}

// Defining the Playground handler
func playgroundHandler() gin.HandlerFunc {
	h := playground.Handler("GraphQL", "/query")

	return func(c *gin.Context) {
		h.ServeHTTP(c.Writer, c.Request)
	}
}

// Server is the cli command that runs our main web server
func Server() *cli.Command {
	return &cli.Command{
		Name:  "server",
		Usage: "Starts the tim web server",
		Before: func(c *cli.Context) error {
			services.SetupServices(registry.OrmService())
			return nil
		},
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:        "mongo-uri",
				Value:       "",
				Usage:       "Mongo database uri",
				EnvVars:     []string{"TIM_MONGO_URI"},
				Destination: &config.Database.Uri,
			},
			&cli.IntFlag{
				Name:        "server-port",
				Value:       8066,
				Usage:       "Web server port",
				EnvVars:     []string{"TIM_SERVER_PORT", "PORT"},
				Destination: &config.Server.ServerPort,
			},
			&cli.StringFlag{
				Name:        "server-host",
				Value:       "0.0.0.0",
				Usage:       "Web server host",
				EnvVars:     []string{"TIM_SERVER_HOST"},
				Destination: &config.Server.ServerHost,
			},
		},
		Action: func(c *cli.Context) error {
			r := gin.Default()

			if config.Server.Debug {
				gin.SetMode(gin.DebugMode)
			}

			r.Use(gin.Logger())
			r.Use(gin.Recovery())
			r.Use(GinContextToContextMiddleware())

			r.POST("/query", graphqlHandler())

			r.GET("/", playgroundHandler())

			err := r.Run(fmt.Sprintf("%s:%d", config.Server.ServerHost, config.Server.ServerPort))
			if err != nil {
				panic("failed running gin")
			}

			return nil
		},
	}
}
