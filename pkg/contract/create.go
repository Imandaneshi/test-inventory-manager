package contract

import (
	"context"
	"fmt"

	"github.com/kamva/mgm/v3"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"

	"gitlab.com/Imandaneshi/test-inventory-manager/api/graph/model"
	"gitlab.com/Imandaneshi/test-inventory-manager/pkg/entities"
)

func CreateContract(ctx context.Context, request model.CreateContractRequest) (*model.Contract, error) {
	contractColl := mgm.Coll(&entities.Contract{})

	roomsColl := mgm.Coll(&entities.Room{})
	roomNightsColl := mgm.Coll(&entities.RoomNight{})

	objID, err := primitive.ObjectIDFromHex(request.RoomID)
	if err != nil {
		panic(err.Error())
	}

	room := &entities.Room{}

	err = roomsColl.First(bson.M{"_id": objID}, room)

	if err != nil {
		if err == mongo.ErrNoDocuments {
			return nil, fmt.Errorf("room_not_found")
		}
		panic(err.Error())
	}

	newContract := &entities.Contract{
		RoomID:    objID,
		Active:    request.Active,
		StartDate: request.StartDate,
		EndDate:   request.EndDate,
	}

	err = contractColl.Create(newContract)
	if err != nil {
		return nil, err
	}

	start := request.StartDate
	end := request.EndDate

	var nights []interface{}

	for d := start; d.After(end) == false; d = d.AddDate(0, 0, 1) {
		nights = append(nights, &entities.RoomNight{
			RoomID:     room.ID,
			ContractID: newContract.ID,
			Active:     false,
			Inventory:  uint64(request.Inventory),
			Date:       d,
		})
	}

	_, err = roomNightsColl.InsertMany(ctx, nights)
	if err != nil {
		return nil, err
	}

	return &model.Contract{
		ID:        newContract.ID.Hex(),
		Active:    newContract.Active,
		StartDate: newContract.StartDate,
		EndDate:   newContract.EndDate,
	}, nil
}
