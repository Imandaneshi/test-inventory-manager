package directives

import (
	"context"
	"fmt"

	"github.com/99designs/gqlgen/graphql"
	"github.com/gin-gonic/gin"
	"github.com/kamva/mgm/v3"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"

	"gitlab.com/Imandaneshi/test-inventory-manager/pkg/entities"
	"gitlab.com/Imandaneshi/test-inventory-manager/pkg/services"
)

func GinContextFromContext(ctx context.Context) (*gin.Context, error) {
	ginContext := ctx.Value("GinContextKey")
	if ginContext == nil {
		err := fmt.Errorf("could not retrieve gin.Context")
		return nil, err
	}

	gc, ok := ginContext.(*gin.Context)
	if !ok {
		err := fmt.Errorf("gin.Context has wrong type")
		return nil, err
	}
	return gc, nil
}

func CheckAuthHeader(gc *gin.Context) *entities.UserToken {
	authHeader := gc.GetHeader("Authorization")

	if authHeader == "" {
		return nil
	}

	token := &entities.UserToken{}

	services.GetOrmService() // Load db service

	userTokenColl := mgm.Coll(&entities.UserToken{})

	err := userTokenColl.First(bson.M{"token": authHeader}, token)

	if err != nil {
		if err == mongo.ErrNoDocuments {
			return nil
		}
		panic(err.Error())
	}
	return token
}

func AdminCheck(ctx context.Context, obj interface{}, next graphql.Resolver) (res interface{}, err error) {
	gc, err := GinContextFromContext(ctx)
	if err != nil {
		panic(err.Error())
	}

	token := CheckAuthHeader(gc)

	if token == nil {
		return nil, fmt.Errorf("access_denied")
	}

	user := &entities.User{}

	services.GetOrmService() // Load db service

	userColl := mgm.Coll(&entities.User{})

	err = userColl.First(bson.M{"_id": token.UserID}, user)

	if err != nil {
		if err == mongo.ErrNoDocuments {
			return nil, fmt.Errorf("access_denied")
		}
		panic(err.Error())
	}

	if user.IsAdmin == false {
		return nil, fmt.Errorf("access_denied")
	}

	return next(ctx)
}

func Auth(ctx context.Context, _ interface{}, next graphql.Resolver) (res interface{}, err error) {
	gc, err := GinContextFromContext(ctx)
	if err != nil {
		panic(err.Error())
	}

	token := CheckAuthHeader(gc)

	if token == nil {
		return nil, fmt.Errorf("access_denied")
	}

	gc.Set("userId", token.UserID.Hex())

	return next(ctx)
}
