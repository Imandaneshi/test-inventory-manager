package entities

import (
	"time"

	"github.com/kamva/mgm/v3"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type RoomNight struct {
	mgm.DefaultModel `bson:",inline"`
	RoomID           primitive.ObjectID `bson:"room_id"`
	ContractID       primitive.ObjectID `bson:"contract_id"`
	Active           bool               `bson:"active"`
	Inventory        uint64             `bson:"inventory"`
	Date             time.Time          `bson:"date"`
}

func (model *RoomNight) CollectionName() string {
	return "room_nights"
}
