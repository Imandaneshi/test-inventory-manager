package entities

import (
	"time"

	"github.com/kamva/mgm/v3"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Order struct {
	mgm.DefaultModel `bson:",inline"`
	RoomID           primitive.ObjectID `bson:"room_id"`
	ContractID       primitive.ObjectID `bson:"contract_id"`
	UserID           primitive.ObjectID `bson:"user_id"`
	TotalPrice       uint64             `bson:"total_price"`
	Checkin          time.Time          `bson:"checkin"`
	Checkout         time.Time          `bson:"checkout"`
}

func (model *Order) CollectionName() string {
	return "orders"
}
