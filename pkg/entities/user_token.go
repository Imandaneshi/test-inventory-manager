package entities

import (
	"time"

	"github.com/kamva/mgm/v3"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type UserToken struct {
	mgm.DefaultModel `bson:",inline"`
	UserID           primitive.ObjectID `bson:"user_id"`
	Token            string             `bson:"token"`
	Expire           time.Time          `bson:"expire"`
}

func (model *UserToken) CollectionName() string {
	return "user_tokens"
}
