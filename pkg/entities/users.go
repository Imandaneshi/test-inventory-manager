package entities

import "github.com/kamva/mgm/v3"

type User struct {
	mgm.DefaultModel `bson:",inline"`
	FullName         string `bson:"name"`
	Email            string `bson:"email"`
	Password         string `bson:"password"`
	IsAdmin          bool   `bson:"is_admin"`
	TotalOrders      uint64 `bson:"total_orders"`
}

func (model *User) CollectionName() string {
	return "users"
}
