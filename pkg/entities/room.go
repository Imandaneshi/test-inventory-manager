package entities

import "github.com/kamva/mgm/v3"

type Room struct {
	mgm.DefaultModel `bson:",inline"`
	Name             string `bson:"name"`
	Active           bool   `bson:"active"`
	Price            uint64 `bson:"price"`
	Complex          string `bson:"complex"`
}

func (model *Room) CollectionName() string {
	return "rooms"
}
