package entities

import (
	"time"

	"github.com/kamva/mgm/v3"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Contract struct {
	mgm.DefaultModel `bson:",inline"`
	RoomID           primitive.ObjectID `bson:"room_id"`
	Active           bool               `bson:"active"`
	StartDate        time.Time          `bson:"start_date"`
	EndDate          time.Time          `bson:"end_date"`
}

func (model *Contract) CollectionName() string {
	return "contracts"
}
