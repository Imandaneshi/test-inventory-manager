package entities

import (
	"time"

	"github.com/kamva/mgm/v3"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type OrderItem struct {
	mgm.DefaultModel `bson:",inline"`
	RoomID           primitive.ObjectID `bson:"room_id"`
	OrderID          primitive.ObjectID `bson:"order_id"`
	ContractID       primitive.ObjectID `bson:"contract_id"`
	UserName         string             `bson:"user_name"`
	Price            uint64             `bson:"total_price"`
	Night            time.Time          `bson:"night"`
}

func (model *OrderItem) CollectionName() string {
	return "order_items"
}
