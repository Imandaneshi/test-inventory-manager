package services

import (
	"github.com/sarulabs/di"

	"gitlab.com/Imandaneshi/test-inventory-manager/pkg/services/registry"
)

var container di.Container

func SetupServices(services ...*di.Def) {
	builder, _ := di.NewBuilder()

	for _, service := range services {
		err := builder.Add(*service)
		if err != nil {
			panic("error on add definition to the container")
		}
	}

	container = builder.Build()
}

func GetOrmService() bool {
	return container.Get(registry.OrmServiceDefinition).(bool)
}
