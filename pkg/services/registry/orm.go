package registry

import (
	"github.com/kamva/mgm/v3"
	"github.com/sarulabs/di"
	"go.mongodb.org/mongo-driver/mongo/options"

	"gitlab.com/Imandaneshi/test-inventory-manager/config"
)

func OrmService() *di.Def {
	return &di.Def{
		Name: "orm",
		Build: func(ctn di.Container) (interface{}, error) {
			dsn := config.Database.Uri
			err := mgm.SetDefaultConfig(nil, "tim", options.Client().ApplyURI(dsn))
			return true, err
		},
	}
}
