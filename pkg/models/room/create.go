package room

import (
	"context"

	"github.com/kamva/mgm/v3"

	"gitlab.com/Imandaneshi/test-inventory-manager/api/graph/model"
	"gitlab.com/Imandaneshi/test-inventory-manager/pkg/entities"
)

func CreateRoom(ctx context.Context, request model.CreateRoomRequest) (*model.Room, error) {
	roomsColl := mgm.Coll(&entities.Room{})

	newRoom := &entities.Room{
		Name:    request.Name,
		Active:  request.Active,
		Price:   uint64(request.Price),
		Complex: request.Complex,
	}

	err := roomsColl.Create(newRoom)

	if err != nil {
		panic(err.Error())
	}
	return &model.Room{
		ID:      newRoom.ID.Hex(),
		Name:    newRoom.Name,
		Active:  newRoom.Active,
		Price:   int(newRoom.Price),
		Complex: newRoom.Complex,
	}, nil
}
