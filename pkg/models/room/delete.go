package room

import (
	"context"
	"fmt"

	"github.com/kamva/mgm/v3"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"

	"gitlab.com/Imandaneshi/test-inventory-manager/api/graph/model"
	"gitlab.com/Imandaneshi/test-inventory-manager/pkg/entities"
)

func DeleteRoom(ctx context.Context, request model.DeleteRoomRequest) (bool, error) {
	roomsColl := mgm.Coll(&entities.Room{})

	objID, err := primitive.ObjectIDFromHex(request.ID)
	if err != nil {
		panic(err.Error())
	}

	room := &entities.Room{}

	err = roomsColl.First(bson.M{"_id": objID}, room)

	if err != nil {
		if err == mongo.ErrNoDocuments {
			return false, fmt.Errorf("room_not_found")
		}
		panic(err.Error())
	}

	err = roomsColl.Delete(room)
	if err != nil {
		panic(err.Error())
	}

	return true, nil
}
