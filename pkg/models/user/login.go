package user

import (
	"context"
	"errors"

	"github.com/kamva/mgm/v3"
	"go.mongodb.org/mongo-driver/bson"
	"golang.org/x/crypto/bcrypt"

	"gitlab.com/Imandaneshi/test-inventory-manager/api/graph/model"
	"gitlab.com/Imandaneshi/test-inventory-manager/pkg/entities"
	usertoken "gitlab.com/Imandaneshi/test-inventory-manager/pkg/models/user_token"
	"gitlab.com/Imandaneshi/test-inventory-manager/pkg/services"
)

func Login(ctx context.Context, request *model.LoginRequest) (*model.UserTokenRes, error) {
	services.GetOrmService() // Load db service

	userColl := mgm.Coll(&entities.User{})

	count, err := userColl.CountDocuments(ctx, bson.M{"email": request.Email})

	if err != nil {
		panic(err.Error())
	}

	if count == 0 {
		return nil, errors.New("email_does_not_exists")
	}

	user := &entities.User{}

	err = userColl.First(bson.M{"email": request.Email}, user)
	if err != nil {
		panic(err.Error())
	}

	err = bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(request.Password))

	if err != nil {
		return nil, errors.New("invalid_password")
	}

	newToken := usertoken.CreateUserToken(user)

	return &model.UserTokenRes{
		ID:       user.ID.Hex(),
		FullName: user.FullName,
		Email:    user.Email,
		Token:    newToken.Token,
	}, nil
}
