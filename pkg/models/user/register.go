package user

import (
	"context"
	"errors"

	"github.com/kamva/mgm/v3"
	"go.mongodb.org/mongo-driver/bson"
	"golang.org/x/crypto/bcrypt"

	"gitlab.com/Imandaneshi/test-inventory-manager/api/graph/model"
	"gitlab.com/Imandaneshi/test-inventory-manager/pkg/entities"
	usertoken "gitlab.com/Imandaneshi/test-inventory-manager/pkg/models/user_token"
	"gitlab.com/Imandaneshi/test-inventory-manager/pkg/services"
)

func Register(ctx context.Context, request *model.RegisterRequest) (*model.UserTokenRes, error) {
	services.GetOrmService() // Load db service

	userColl := mgm.Coll(&entities.User{})

	count, err := userColl.CountDocuments(ctx, bson.M{"email": request.Email})

	if err != nil {
		return nil, errors.New("internal server error")
	}

	if count != 0 {
		return nil, errors.New("email_already_exists")
	}

	// Hashing the password with the default cost of 10
	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(request.Password), bcrypt.DefaultCost)
	if err != nil {
		panic(err.Error())
	}

	newUser := &entities.User{
		FullName:    request.FullName,
		Email:       request.Email,
		IsAdmin:     true,
		Password:    string(hashedPassword),
		TotalOrders: 0,
	}

	err = userColl.Create(newUser)

	if err != nil {
		panic(err.Error())
	}

	newToken := usertoken.CreateUserToken(newUser)

	return &model.UserTokenRes{
		ID:       newUser.ID.Hex(),
		FullName: newUser.FullName,
		Email:    newUser.Email,
		Token:    newToken.Token,
	}, nil
}
