package usertoken

import (
	"time"

	"github.com/google/uuid"
	"github.com/kamva/mgm/v3"

	"gitlab.com/Imandaneshi/test-inventory-manager/pkg/entities"
	"gitlab.com/Imandaneshi/test-inventory-manager/pkg/services"
)

func CreateUserToken(user *entities.User) *entities.UserToken {
	services.GetOrmService() // Load db service

	userTokenColl := mgm.Coll(&entities.UserToken{})

	token := uuid.New().String()

	userToken := &entities.UserToken{
		UserID: user.ID,
		Token:  token,
		Expire: time.Now().Add(2 * 24 * time.Hour),
	}

	err := userTokenColl.Create(userToken)
	if err != nil {
		panic(err.Error())
	}

	return userToken
}
