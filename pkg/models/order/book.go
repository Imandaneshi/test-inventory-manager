package order

import (
	"context"
	"fmt"
	"github.com/kamva/mgm/v3"
	"gitlab.com/Imandaneshi/test-inventory-manager/api/graph/model"
	"gitlab.com/Imandaneshi/test-inventory-manager/pkg/directives"
	"gitlab.com/Imandaneshi/test-inventory-manager/pkg/entities"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

func BookOrder(ctx context.Context, request *model.BookOrderRequest) (*model.Order, error) {

	diff := request.Checkout.Sub(request.Checkin)
	days := int(diff.Hours() / 24)

	gc, err := directives.GinContextFromContext(ctx)
	if err != nil {
		panic(err.Error())
	}

	userIDVal := gc.GetString("userId")

	if userIDVal == "" {
		panic("no user key set")
	}

	userID, err := primitive.ObjectIDFromHex(userIDVal)
	if err != nil {
		panic(err.Error())
	}

	user := &entities.User{}

	userColl := mgm.Coll(&entities.User{})

	err = userColl.First(bson.M{"_id": userID}, user)

	if err != nil {
		if err == mongo.ErrNoDocuments {
			return nil, fmt.Errorf("user_not_exists")
		}
		panic(err.Error())
	}

	roomsColl := mgm.Coll(&entities.Room{})

	objID, err := primitive.ObjectIDFromHex(request.RoomID)
	if err != nil {
		panic(err.Error())
	}

	room := &entities.Room{}

	err = roomsColl.First(bson.M{"_id": objID}, room)

	if err != nil {
		if err == mongo.ErrNoDocuments {
			return nil, fmt.Errorf("room_not_found")
		}
		panic(err.Error())
	}

	roomNightsColl := mgm.Coll(&entities.RoomNight{})

	res := []entities.RoomNight{}

	err = roomNightsColl.SimpleFind(&res, bson.M{
		"date":      bson.M{"$gte": request.Checkin.UTC(), "$lt": request.Checkout.UTC()},
		"room_id":   room.ID,
		"inventory": bson.M{"$gte": 1},
	})

	if err != nil {
		panic(err.Error())
	}

	if len(res) != days {
		return nil, fmt.Errorf("room_not_available")
	}

	var roomNightIds []primitive.ObjectID

	for _, roomNight := range res {
		roomNightIds = append(roomNightIds, roomNight.ID)
	}

	_, err = roomNightsColl.UpdateMany(ctx, bson.M{
		"date": bson.M{"$gte": request.Checkin.UTC(), "$lt": request.Checkout.UTC()},
	}, bson.M{"$inc": bson.M{"inventory": -1}})

	if err != nil {
		panic(err.Error())
	}

	ordersColl := mgm.Coll(&entities.Order{})

	newOrder := &entities.Order{
		RoomID:     room.ID,
		ContractID: res[0].ContractID,
		UserID:     user.ID,
		TotalPrice: room.Price * uint64(len(roomNightIds)),
		Checkin:    request.Checkin,
		Checkout:   request.Checkout,
	}

	err = ordersColl.Create(newOrder)

	if err != nil {
		panic(err.Error())
	}

	var items []interface{}

	for d := request.Checkin; d.After(request.Checkout.AddDate(0, 0, -1)) == false; d = d.AddDate(0, 0, 1) {
		items = append(items, &entities.OrderItem{
			RoomID:     room.ID,
			OrderID:    newOrder.ID,
			Price:      room.Price,
			ContractID: res[0].ContractID,
			Night:      d,
		})
	}

	orderItemsColl := mgm.Coll(&entities.RoomNight{})

	_, err = orderItemsColl.InsertMany(ctx, items)
	if err != nil {
		panic(err.Error())
	}

	orderItems := []entities.OrderItem{}

	err = orderItemsColl.SimpleFind(&orderItems, bson.M{"order_id": newOrder.ID})
	if err != nil {
		panic(err.Error())
	}

	orderItemsModels := make([]*model.OrderItem, len(orderItems))

	for index, orderItem := range orderItems {
		orderItemsModels[index] = &model.OrderItem{
			ID:    orderItem.ID.Hex(),
			Price: int(orderItem.Price),
			Night: orderItem.Night,
		}
	}

	return &model.Order{
		ID:         newOrder.ID.Hex(),
		TotalPrice: int(newOrder.TotalPrice),
		Checkin:    newOrder.Checkin,
		Checkout:   newOrder.Checkout,
		Items:      orderItemsModels,
	}, nil
}
