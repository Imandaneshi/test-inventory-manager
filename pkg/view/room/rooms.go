package room

import (
	"context"
	"github.com/kamva/mgm/v3"
	"github.com/xorcare/pointer"
	"go.mongodb.org/mongo-driver/bson"

	"gitlab.com/Imandaneshi/test-inventory-manager/api/graph/model"
	"gitlab.com/Imandaneshi/test-inventory-manager/pkg/entities"
	"gitlab.com/Imandaneshi/test-inventory-manager/pkg/services"
)

type CustomRoom struct {
	mgm.DefaultModel `bson:",inline"`
	Name             string `bson:"name"`
	Active           bool   `bson:"active"`
	Price            uint64 `bson:"price"`
	Complex          string `bson:"complex"`
	IsAvailable      bool   `bson:"is_available"`
}

func Rooms(ctx context.Context, request model.RoomsRequest) ([]*model.Room, error) {
	services.GetOrmService() // Load db service

	roomNightsColl := mgm.Coll(&entities.RoomNight{})

	var res []CustomRoom

	diff := request.Checkout.Sub(request.Checkin)
	days := int(diff.Hours() / 24)

	cur, err := roomNightsColl.Aggregate(ctx, []bson.M{
		{
			"$match": bson.M{
				"date": bson.M{"$gte": request.Checkin.UTC(), "$lt": request.Checkout.UTC()},
			},
		},
		{
			"$group": bson.M{
				"_id":              "$room_id",
				"inventory":        bson.M{"$min": "$inventory"},
				"available_nights": bson.M{"$sum": 1},
			},
		},
		{
			"$lookup": bson.M{
				"from":         "rooms",
				"localField":   "_id",
				"foreignField": "_id",
				"as":           "room_info",
			},
		},
		{
			"$addFields": bson.M{"room_info": bson.M{"$arrayElemAt": bson.A{"$room_info", 0}}},
		},
		{
			"$match": bson.M{
				"room_info.active": true,
			},
		},
		{"$addFields": bson.M{"is_available": bson.M{"$and": bson.A{
			bson.M{"$eq": bson.A{"$available_nights", days}},
			bson.M{"$gte": bson.A{"$inventory", 1}}}},
		}},
		{
			"$project": bson.M{
				"_id":          "$_id",
				"name":         "$room_info.name",
				"complex":      "$room_info.complex",
				"price":        "$room_info.price",
				"active":       "$room_info.active",
				"is_available": "$is_available",
			},
		}})

	if err != nil {
		panic(err.Error())
	}

	if err = cur.All(ctx, &res); err != nil {
		panic(err.Error())
	}
	if err := cur.Close(ctx); err != nil {
		panic(err.Error())
	}

	rooms := make([]*model.Room, len(res))

	if len(res) == 0 {
		return rooms, nil
	}

	for index, room := range res {
		rooms[index] = &model.Room{
			ID:          room.ID.Hex(),
			Name:        room.Name,
			Active:      false,
			Price:       int(room.Price),
			Complex:     room.Complex,
			IsAvailable: pointer.Bool(room.IsAvailable),
		}
	}

	return rooms, nil
}
