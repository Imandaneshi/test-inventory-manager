export GO111MODULE=on
export GOPROXY=https://goproxy.io

format: ## Format go code with goimports
	go install github.com/rinchsan/gosimports/cmd/gosimports@latest
	find . -name \*.go -exec gosimports -local gitlab.com/Imandaneshi/ -w {} \;

generate-api:
	cd ./api/graph && go run github.com/99designs/gqlgen
	make format