package graph

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"

	"gitlab.com/Imandaneshi/test-inventory-manager/api/graph/model"
	"gitlab.com/Imandaneshi/test-inventory-manager/pkg/models/room"
	room2 "gitlab.com/Imandaneshi/test-inventory-manager/pkg/view/room"
)

func (r *mutationResolver) CreateRoom(ctx context.Context, request model.CreateRoomRequest) (*model.Room, error) {
	return room.CreateRoom(ctx, request)
}

func (r *mutationResolver) DeleteRoom(ctx context.Context, request model.DeleteRoomRequest) (bool, error) {
	return room.DeleteRoom(ctx, request)
}

func (r *queryResolver) Rooms(ctx context.Context, request model.RoomsRequest) ([]*model.Room, error) {
	return room2.Rooms(ctx, request)
}
