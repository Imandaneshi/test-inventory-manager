package graph

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"

	"gitlab.com/Imandaneshi/test-inventory-manager/api/graph/model"
	"gitlab.com/Imandaneshi/test-inventory-manager/pkg/contract"
)

func (r *mutationResolver) CreateContract(ctx context.Context, request model.CreateContractRequest) (*model.Contract, error) {
	return contract.CreateContract(ctx, request)
}
