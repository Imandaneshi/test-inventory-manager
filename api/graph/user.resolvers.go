package graph

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"

	"gitlab.com/Imandaneshi/test-inventory-manager/api/graph/model"
	"gitlab.com/Imandaneshi/test-inventory-manager/pkg/models/user"
)

func (r *mutationResolver) Register(ctx context.Context, request *model.RegisterRequest) (*model.UserTokenRes, error) {
	return user.Register(ctx, request)
}

func (r *mutationResolver) Login(ctx context.Context, request *model.LoginRequest) (*model.UserTokenRes, error) {
	return user.Login(ctx, request)
}
