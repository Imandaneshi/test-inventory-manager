package graph

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"
	"fmt"

	"gitlab.com/Imandaneshi/test-inventory-manager/api/graph/generated"
	"gitlab.com/Imandaneshi/test-inventory-manager/api/graph/model"
)

func (r *mutationResolver) Ping(ctx context.Context) (*model.PingResponse, error) {
	panic(fmt.Errorf("not implemented"))
}

func (r *queryResolver) Ping(ctx context.Context) (*model.PingResponse, error) {
	return &model.PingResponse{Pong: true}, nil
}

// Mutation returns generated.MutationResolver implementation.
func (r *Resolver) Mutation() generated.MutationResolver { return &mutationResolver{r} }

// Query returns generated.QueryResolver implementation.
func (r *Resolver) Query() generated.QueryResolver { return &queryResolver{r} }

type mutationResolver struct{ *Resolver }
type queryResolver struct{ *Resolver }
