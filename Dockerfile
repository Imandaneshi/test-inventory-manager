    # 1. using golang alpine image to build server side codes
FROM golang:alpine as goBuilder

ADD . /code

WORKDIR /code

RUN go mod download

WORKDIR /code/cmd/tim

RUN go install

# 2. final image with no dependency, just binaries
FROM alpine

# https://stackoverflow.com/questions/34729748/installed-go-binary-not-found-in-path-on-alpine-linux-docker/35613430#35613430
RUN mkdir /lib64 && ln -s /lib/libc.musl-x86_64.so.1 /lib64/ld-linux-x86-64.so.2

WORKDIR /app

COPY --from=goBuilder /go/bin/tim /usr/local/bin/tim
EXPOSE 8066

CMD ["tim", "server"]